// Pacakge main entrypoint
package main

import (
	"encoding/hex"
	"encoding/json"
	"flag"
	"fmt"
	"image/color"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/golang/freetype"
	"github.com/golang/freetype/truetype"
	"gitlab.eientei.org/eientei/captnha/internal/generator/rotations"
	"gitlab.eientei.org/eientei/captnha/internal/generator/rotations/renderer"
	"gitlab.eientei.org/eientei/captnha/internal/generator/rotations/renderer/sdf"
	"gitlab.eientei.org/eientei/captnha/service/captnha"
	"golang.org/x/image/font"
)

var buildParamVersion string

func loadfont(font string, size float64) (face font.Face, err error) {
	var bs []byte

	if font == "" {
		bs = rotations.TTF
	} else {
		bs, err = os.ReadFile(font)
		if err != nil {
			return
		}
	}

	data, err := freetype.ParseFont(bs)
	if err != nil {
		return
	}

	return truetype.NewFace(data, &truetype.Options{
		Size: size,
	}), nil
}

func parsecolor(rendercolor string) (color.RGBA, error) {
	var bs []byte

	var err error

	if len(rendercolor) == 3 {
		bs, err = hex.DecodeString(rendercolor + "0")
	} else {
		bs, err = hex.DecodeString(rendercolor)
	}

	if err != nil {
		return color.RGBA{}, err
	}

	switch len(rendercolor) {
	case 3:
		return color.RGBA{
			R: ((bs[0]>>4)&0xf)<<4 | 0xf,
			G: (bs[0]&0xf)<<4 | 0xf,
			B: ((bs[1]>>4)&0xf)<<4 | 0xf,
			A: 0xFF,
		}, err
	case 4:
		return color.RGBA{
			R: ((bs[0]>>4)&0xf)<<4 | 0xf,
			G: (bs[0]&0xf)<<4 | 0xf,
			B: ((bs[1]>>4)&0xf)<<4 | 0xf,
			A: (bs[1]&0xf)<<4 | 0xf,
		}, err
	case 6:
		return color.RGBA{
			R: bs[0],
			G: bs[1],
			B: bs[2],
			A: 0xff,
		}, err
	case 8:
		return color.RGBA{
			R: bs[0],
			G: bs[1],
			B: bs[2],
			A: bs[3],
		}, err
	default:
		return color.RGBA{}, fmt.Errorf("invalid color hex code")
	}
}

func main() {
	renconfig := sdf.DefaultConfig()

	genconfig := rotations.DefaultConfig()

	var directory, listen, foreground, background, rendercolor, bigfont, smallfont string

	var smallfontsize, bigfontsize float64

	var cleanup, bandwidthPeriod time.Duration

	var bandwidthBytes int

	origin := "[0, 0, -2]"

	var version string

	if buildParamVersion != "" {
		version = " " + buildParamVersion
	}

	flag.Usage = func() {
		_, _ = fmt.Fprintf(flag.CommandLine.Output(), "Usage of %s%s:\n", os.Args[0], version)

		flag.PrintDefaults()
	}

	flag.StringVar(
		&directory,
		"directory",
		"captnhas",
		"directory for stored files",
	)
	flag.StringVar(
		&listen,
		"listen",
		":8080",
		"listen HTTP port",
	)
	flag.DurationVar(
		&cleanup,
		"cleanup",
		time.Minute*5,
		"file cleanup time",
	)
	flag.DurationVar(
		&bandwidthPeriod,
		"bandwidth-period",
		time.Second,
		"bandwidth period for serving images",
	)
	flag.IntVar(
		&bandwidthBytes,
		"bandwidth-bytes",
		0,
		"bandwidth bytes allowed within bandwidth period for serving images, 0 to disable",
	)
	flag.IntVar(
		&genconfig.MaxBoxes,
		"rotations-maxboxes",
		genconfig.MaxBoxes,
		"maximum number of boxes, capped at [3..10]",
	)
	flag.IntVar(
		&genconfig.MinBoxes,
		"rotations-minboxes",
		genconfig.MinBoxes,
		"minimum number of boxes, capped at [3..10]",
	)
	flag.IntVar(
		&genconfig.Variation,
		"rotations-variation",
		genconfig.Variation,
		"direction variation factor",
	)
	flag.IntVar(
		&genconfig.CodeLength,
		"rotations-code-length",
		genconfig.CodeLength,
		"digits in sum components",
	)
	flag.IntVar(
		&genconfig.DegreesOfFreedom,
		"rotations-dof",
		genconfig.DegreesOfFreedom,
		"direction degrees of freedom",
	)
	flag.IntVar(
		&genconfig.Gap,
		"rotations-gap",
		genconfig.Gap,
		"vertical gap for codes/description in pixels",
	)
	flag.IntVar(
		&genconfig.Width,
		"rotations-width",
		genconfig.Width,
		"width of the figure, should be square",
	)
	flag.IntVar(
		&genconfig.Height,
		"rotations-height",
		genconfig.Height,
		"height of the figure, should be square",
	)
	flag.Float64Var(
		&genconfig.WireWidth,
		"rotations-wire",
		genconfig.WireWidth,
		"wireframe width",
	)
	flag.StringVar(
		&foreground,
		"rotations-foreground",
		"000000ff",
		"foreground color, RGBA hex code",
	)
	flag.StringVar(
		&background,
		"rotations-background",
		"ffffffff",
		"background color, RGBA hex code",
	)
	flag.StringVar(
		&rendercolor,
		"rotations-rendercolor",
		"000000ff",
		"wireframe render color",
	)
	flag.StringVar(
		&bigfont,
		"rotations-bigfont",
		"",
		"path to ttf file",
	)
	flag.StringVar(
		&smallfont,
		"rotations-smallfont",
		"",
		"path to ttf file",
	)
	flag.Float64Var(
		&smallfontsize,
		"rotations-smallfontsize",
		float64(genconfig.Gap)*0.4,
		"small font size at 72 dpi",
	)
	flag.Float64Var(
		&bigfontsize,
		"rotations-bigfontsize",
		float64(genconfig.Gap)*0.8,
		"big font size at 72 dpi",
	)
	flag.Float64Var(
		&genconfig.NoiseThreshold,
		"rotations-noise",
		genconfig.NoiseThreshold,
		"noise threshold, (0 .. 1.0]",
	)
	flag.Float64Var(
		&genconfig.Tolerance,
		"rotations-tolerance",
		genconfig.Tolerance,
		"rotation tolerance, degrees",
	)
	flag.IntVar(
		&renconfig.Workers,
		"rotations-sdf-workers",
		renconfig.Workers,
		"sdf renderer workers",
	)
	flag.IntVar(
		&renconfig.Iterations,
		"rotations-sdf-iterations",
		renconfig.Iterations,
		"sdf renderer max iterations",
	)
	flag.Float64Var(
		&renconfig.AliasStart,
		"rotations-sdf-alias-start",
		renconfig.AliasStart,
		"sdf renderer upper threshold",
	)
	flag.Float64Var(
		&renconfig.AliasStop,
		"rotations-sdf-alias-stop",
		renconfig.AliasStop,
		"sdf renderer lower threshold",
	)
	flag.Float64Var(
		&renconfig.Perspective,
		"rotations-sdf-perspective",
		renconfig.Perspective,
		"sdf renderer perspective factor",
	)
	flag.Float64Var(
		&renconfig.Distance,
		"rotations-sdf-distance",
		renconfig.Distance,
		"sdf renderer distance from origin",
	)
	flag.StringVar(
		&origin,
		"rotations-sdf-origin",
		origin,
		"sdf renderer origin vector",
	)

	flag.Parse()

	switch {
	case genconfig.DegreesOfFreedom > 3:
		log.Fatal("degrees of freedom cannot exceed 3")
	case genconfig.DegreesOfFreedom < 1:
		log.Fatal("degrees of freedom cannot be less than 1")
	}

	var err error

	genconfig.Background, err = parsecolor(background)
	if err != nil {
		panic(err)
	}

	genconfig.Foreground, err = parsecolor(foreground)
	if err != nil {
		panic(err)
	}

	rc, err := parsecolor(rendercolor)
	if err != nil {
		panic(err)
	}

	genconfig.RendererColor = func(u uint8) color.Color {
		return color.RGBA{
			R: rc.R,
			G: rc.G,
			B: rc.B,
			A: uint8(uint32(rc.A) * uint32(u) / 255),
		}
	}

	genconfig.BigFont, err = loadfont(bigfont, bigfontsize)
	if err != nil {
		panic(err)
	}

	genconfig.SmallFont, err = loadfont(smallfont, smallfontsize)
	if err != nil {
		panic(err)
	}

	var originarr []float64

	err = json.Unmarshal(([]byte)(origin), &originarr)
	if err != nil {
		panic(err)
	}

	if len(originarr) != 3 {
		log.Fatal("origin array must have exactly three components")
	}

	renconfig.Origin = renderer.Vector3{originarr[0], originarr[1], originarr[2]}

	err = os.MkdirAll(directory, 0777)
	if err != nil {
		panic(err)
	}

	r, err := sdf.NewRenderer(renconfig)
	if err != nil {
		panic(err)
	}

	genconfig.Renderer = r

	generator, err := rotations.NewGenerator(genconfig)
	if err != nil {
		panic(err)
	}

	handler, err := captnha.NewCAPTNHA(&captnha.Config{
		Generator:       generator,
		Directory:       directory,
		Cleanup:         cleanup,
		BandwidthPeriod: bandwidthPeriod,
		BandwidthBytes:  bandwidthBytes,
	})
	if err != nil {
		panic(err)
	}

	err = http.ListenAndServe(listen, handler)
	if err != nil {
		panic(err)
	}
}
