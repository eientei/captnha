// Package generator defines interface for CAPTNHA generators
package generator

import "io"

// Generator renders complete image to io.Writer and returns expected code
type Generator interface {
	Generate(out io.Writer) (code string, err error)
}
