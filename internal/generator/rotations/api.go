// Package rotations provides CAPTNHA generator using 3D rotations quiz
package rotations

import (
	_ "embed"
	"image"
	"image/color"
	"image/draw"
	"image/png"
	"io"
	"math"
	"math/rand"
	"sort"
	"strconv"
	"strings"

	"github.com/fogleman/gg"
	"github.com/golang/freetype/truetype"
	"gitlab.eientei.org/eientei/captnha/internal/generator"
	"gitlab.eientei.org/eientei/captnha/internal/generator/rotations/renderer"
	"gitlab.eientei.org/eientei/captnha/internal/generator/rotations/renderer/sdf"
	"golang.org/x/image/font"
)

// TTF embedded font data
//
//go:embed anonymous_pro.ttf
var TTF []byte

// Config parameters
type Config struct {
	BigFont          font.Face
	SmallFont        font.Face
	Background       color.Color
	Foreground       color.Color
	RendererColor    func(uint8) color.Color
	Renderer         renderer.Renderer
	WireWidth        float64
	NoiseThreshold   float64
	Tolerance        float64
	CodeLength       int
	DegreesOfFreedom int
	Variation        int
	MinBoxes         int
	MaxBoxes         int
	Width            int
	Height           int
	Gap              int
}

// DefaultConfig returns new default config
func DefaultConfig() *Config {
	return &Config{
		BigFont:    nil,
		SmallFont:  nil,
		Background: color.White,
		Foreground: color.Black,
		RendererColor: func(u uint8) color.Color {
			return color.Gray{
				Y: 255 - u,
			}
		},
		Renderer:         nil,
		WireWidth:        0.005,
		NoiseThreshold:   0.0,
		Tolerance:        10,
		CodeLength:       4,
		DegreesOfFreedom: 3,
		Variation:        10,
		MinBoxes:         8,
		MaxBoxes:         10,
		Width:            200,
		Height:           200,
		Gap:              50,
	}
}

// NewGenerator returns new generator instance
func NewGenerator(config *Config) (g generator.Generator, err error) {
	if config.BigFont == nil || config.SmallFont == nil {
		var f *truetype.Font

		f, err = truetype.Parse(TTF)
		if err != nil {
			return nil, err
		}

		if config.BigFont == nil {
			config.BigFont = truetype.NewFace(f, &truetype.Options{
				Size: 0.8 * float64(config.Gap),
			})
		}

		if config.SmallFont == nil {
			config.SmallFont = truetype.NewFace(f, &truetype.Options{
				Size: 0.4 * float64(config.Gap),
			})
		}
	}

	if config.Renderer == nil {
		config.Renderer, err = sdf.NewRenderer(sdf.DefaultConfig())
		if err != nil {
			return nil, err
		}
	}

	return &generatorCAPTNHA{
		Config:        *config,
		tolerance:     config.Tolerance * (math.Pi / 180),
		codelengthpow: int(math.Pow10(config.CodeLength)),
	}, nil
}

type generatorCAPTNHA struct {
	Config
	tolerance     float64
	codelengthpow int
}

func randLocalGolomb(n, max int) (res []int) {
	perm := make(map[int]struct{})

	for i := 0; i < n; i++ {
		c := rand.Int() % max

		for {
			if _, ok := perm[c]; !ok {
				break
			}

			c = (c + 1) % max
		}

		res = append(res, c)

		perm[c] = struct{}{}

		for _, r := range res {
			perm[r+c] = struct{}{}
		}
	}

	return
}

func (g *generatorCAPTNHA) formatCode(i int) string {
	a := strconv.Itoa(i)

	return strings.Repeat("0", g.CodeLength-len(a)) + a
}

func (g *generatorCAPTNHA) genboxes(minboxes, maxboxes, maxdim int) (boxes, edges []*renderer.Vector3) {
	boxes = []*renderer.Vector3{
		{
			0, 0, 0,
		},
	}

	var target int

	if maxboxes == minboxes {
		target = maxboxes
	} else {
		target = minboxes + rand.Int()%(maxboxes-minboxes)
	}

	last := boxes[0]
	dir := rand.Int() % 3

	for i := 1; i < target; i++ {
		sw := rand.Int() % (g.Variation * (i + 1))

		if sw != 0 {
			dir = sw % maxdim
		}

		next := last.Copy()

		next[dir] += 1.0
		boxes = append(boxes, next)

		last = next
	}

	edges = append(edges, boxes[0])
	edges = append(edges, boxes[len(boxes)-1])

	return
}

func (g *generatorCAPTNHA) mutate(
	prev *renderer.Topology,
	edges []*renderer.Vector3,
	idx int,
	shift int,
) (topology *renderer.Topology, newedges []*renderer.Vector3) {
	topology = &renderer.Topology{
		Size:  prev.Size,
		Wire:  prev.Wire,
		Boxes: nil,
	}

	for _, b := range prev.Boxes {
		switch {
		case b == edges[(idx+1)%2]:
			topology.Boxes = append(topology.Boxes, b.Copy())
			newedges = append(newedges, topology.Boxes[len(topology.Boxes)-1])
		case b != edges[idx]:
			topology.Boxes = append(topology.Boxes, b.Copy())
		}
	}

	var candpos, candneg []*renderer.Vector3

	for i := 0; i < 3; i++ {
		offpos := *edges[idx]
		offneg := *edges[idx]

		offpos[i]++
		offneg[i]--

		candpos = append(candpos, &offpos)
		candneg = append(candneg, &offneg)
	}

	var prevdir float64

	var prevaxis int

	for _, b := range topology.Boxes {
		for i, c := range candpos {
			if *c == *b {
				prevdir = 1
				prevaxis = i

				break
			}
		}

		for i, c := range candneg {
			if *c == *b {
				prevdir = -1
				prevaxis = i

				break
			}
		}
	}

	res := *edges[idx]

	res[prevaxis] += prevdir
	res[(prevaxis+shift)%3] += prevdir

	for i := 0; i < 3; i++ {
		offpos := res
		offneg := res

		offpos[i]++
		offneg[i]--

		candpos[i] = &offpos
		candneg[i] = &offneg
	}

	var count int

	for _, b := range topology.Boxes {
		if *b == res {
			count = 2

			break
		}

		for _, c := range candpos {
			if *c == *b {
				count++
			}
		}

		for _, c := range candneg {
			if *c == *b {
				count++
			}
		}
	}

	if count >= 2 {
		res[(prevaxis+shift)%3] -= prevdir * 2
	}

	topology.Boxes = append(topology.Boxes, &res)

	newedges = append(newedges, &res)

	return
}

func (g *generatorCAPTNHA) rotate(topology *renderer.Topology) *renderer.Matrix4 {
	mm := renderer.Matrix4Identity()

	t := topology.Tensor()

	eivec, values := t.Eigenspace()

	idx := []int{0, 1, 2}

	sort.Slice(idx, func(j, i int) bool {
		return values[idx[i]] < values[idx[j]]
	})

	tgt := &renderer.Vector3{0, 0, 1}
	cur := (&renderer.Vector3{
		eivec[idx[0]*3],
		eivec[idx[0]*3+1],
		eivec[idx[0]*3+2],
	}).Unit()

	mm = mm.MulMat4(renderer.RotateTo(tgt, cur))

	res := mm.MulVec3(cur)

	minorrot := math.Pi / 44 * math.Copysign(float64(rand.Int()%g.Variation), res[1])
	leastrot := math.Pi / 48 * math.Copysign(float64(rand.Int()%g.Variation), res[2])

	mm = mm.MulMat4(renderer.RotateY(minorrot))
	mm = mm.MulMat4(renderer.RotateX(leastrot))

	if g.tolerance > 0 {
	loop:
		for i := 0; i < 5; i++ {
			sets := mm.EulerAngles()

			diffx := math.Min(math.Abs(sets[0][0]), math.Abs(math.Pi-math.Abs(sets[0][0])))
			diffy := math.Min(math.Abs(sets[0][1]), math.Abs(math.Pi-math.Abs(sets[0][1])))

			if diffx > math.Pi/2 {
				diffx -= math.Pi / 2
			}

			if diffy > math.Pi/2 {
				diffy -= math.Pi / 2
			}

			switch {
			case diffx < g.tolerance || (diffx > math.Pi/4 && diffx < math.Pi/4+g.tolerance):
				mm = mm.MulMat4(renderer.RotateX(math.Copysign(g.tolerance, sets[0][0])))
			case diffy < g.tolerance || (diffy > math.Pi/4 && diffy < math.Pi/4+g.tolerance):
				mm = mm.MulMat4(renderer.RotateY(math.Copysign(g.tolerance, sets[0][1])))
			case diffx > math.Pi/4-g.tolerance && diffx < math.Pi/4:
				mm = mm.MulMat4(renderer.RotateX(math.Copysign(g.tolerance, -sets[0][0])))
			case diffy > math.Pi/4-g.tolerance && diffy < math.Pi/4:
				mm = mm.MulMat4(renderer.RotateY(math.Copysign(g.tolerance, -sets[0][1])))
			default:
				break loop
			}
		}
	}

	mm = mm.MulMat4(renderer.Rotate(2, math.Pi*2*rand.Float64()))

	return mm
}

func (g *generatorCAPTNHA) gentopologies() (answer string, variants []*renderer.Topology) {
	boxes, edges := g.genboxes(g.MinBoxes, g.MaxBoxes, g.DegreesOfFreedom)

	codes := randLocalGolomb(6, g.codelengthpow)

	root := &renderer.Topology{
		Size:  1.0,
		Wire:  g.Config.WireWidth,
		Boxes: boxes,
		Code:  g.formatCode(codes[0]),
	}

	variants = append(variants, root)

	a, _ := g.mutate(root, edges, 0, 1)
	a.Code = g.formatCode(codes[1])

	variants = append(variants, a)

	b, _ := g.mutate(root, edges, 1, 1)
	b.Code = g.formatCode(codes[2])

	variants = append(variants, b)

	c, newedges := g.mutate(root, edges, 0, 2)
	c.Code = g.formatCode(codes[3])

	variants = append(variants, c)

	d, _ := g.mutate(c, newedges, 0, 1)
	d.Code = g.formatCode(codes[4])

	variants = append(variants, d)

	root.Center()
	a.Center()
	b.Center()
	c.Center()
	d.Center()

	rootcopy := *root
	rootcopy.Code = g.formatCode(codes[5])

	variants = append(variants, &rootcopy)

	answer = strconv.Itoa(codes[0] + codes[5])

	rand.Shuffle(len(variants), func(i, j int) {
		variants[i], variants[j] = variants[j], variants[i]
	})

	return
}

func (g *generatorCAPTNHA) Generate(out io.Writer) (code string, err error) {
	im := image.NewRGBA(image.Rect(0, 0, g.Width*2, (g.Height+g.Gap)*3+g.Gap))

	draw.Draw(im, im.Bounds(), image.NewUniform(g.Background), image.Point{}, draw.Src)

	answer, variants := g.gentopologies()

	dc := gg.NewContextForRGBA(im)
	dc.SetColor(g.Foreground)
	dc.SetFontFace(g.BigFont)

	for idx, v := range variants {
		x := (idx % 2) * g.Width
		y := (idx / 2) * (g.Height + g.Gap)

		g.Renderer.Render(
			im.SubImage(image.Rect(x, y+g.Gap, x+g.Width, y+g.Height+g.Gap)).(draw.Image),
			v,
			g.rotate(v),
			g.RendererColor,
		)

		fw, fh := dc.MeasureString(v.Code)

		dc.DrawString(v.Code, float64(x)+(float64(g.Width)-fw)/2, float64(y+g.Height+g.Gap)+float64(g.Gap)-fh/2)
	}

	dc.Stroke()

	dc.SetFontFace(g.SmallFont)

	dc.DrawStringWrapped(
		"Input the sum of two numbers\nwith matching figures",
		float64(g.Width*2),
		float64(g.Gap),
		1,
		1.25,
		float64(g.Width*2),
		1.0,
		gg.AlignCenter,
	)

	dc.Stroke()

	bounds := im.Bounds()

	if g.NoiseThreshold > 0 {
		for y := 0; y < bounds.Dy(); y++ {
			if y > g.Height && y < g.Height+g.Gap {
				continue
			}

			for x := 0; x < bounds.Dx(); x++ {
				if rand.Float64()*2 < g.NoiseThreshold {
					c := im.RGBAAt(x, y)
					c.R = 255 - c.R
					c.G = 255 - c.G
					c.B = 255 - c.B

					im.Set(x, y, c)
				}
			}
		}
	}

	err = png.Encode(out, im)
	if err != nil {
		return
	}

	return answer, nil
}
