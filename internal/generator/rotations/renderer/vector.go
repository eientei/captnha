package renderer

import (
	"fmt"
	"math"
	"sort"
)

// Vector3 3-component vector type
type Vector3 [3]float64

// String implementation
func (v *Vector3) String() string {
	return fmt.Sprintf("{%g, %g, %g}", v[0], v[1], v[2])
}

// Copy returns the copy of a vector
func (v *Vector3) Copy() *Vector3 {
	return &Vector3{
		v[0],
		v[1],
		v[2],
	}
}

// Length returns vector length
func (v *Vector3) Length() float64 {
	return math.Sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2])
}

// Sub performs in-place vector difference
func (v *Vector3) Sub(o *Vector3) *Vector3 {
	v[0] -= o[0]
	v[1] -= o[1]
	v[2] -= o[2]

	return v
}

// Unit performs in-place transformation of a vector into unit vector
func (v *Vector3) Unit() *Vector3 {
	q := v.Length()
	if q == 0 {
		return v
	}

	v[0] /= q
	v[1] /= q
	v[2] /= q

	return v
}

// Scale performs in-place scaling of vector by given factor
func (v *Vector3) Scale(f float64) *Vector3 {
	v[0] *= f
	v[1] *= f
	v[2] *= f

	return v
}

// Add performs in-place vector addition
func (v *Vector3) Add(o *Vector3) *Vector3 {
	v[0] += o[0]
	v[1] += o[1]
	v[2] += o[2]

	return v
}

// Abs performs in-place transformation of vector components into absolute values
func (v *Vector3) Abs() *Vector3 {
	v[0] = math.Abs(v[0])
	v[1] = math.Abs(v[1])
	v[2] = math.Abs(v[2])

	return v
}

// Shift performs in-place shift of each vector component by given scalar
func (v *Vector3) Shift(e float64) *Vector3 {
	v[0] += e
	v[1] += e
	v[2] += e

	return v
}

// Cross returns cross-product with other vector
func (v *Vector3) Cross(o *Vector3) *Vector3 {
	return &Vector3{
		v[1]*o[2] - v[2]*o[1],
		v[2]*o[0] - v[0]*o[2],
		v[0]*o[1] - v[1]*o[0],
	}
}

// Dot returns dot-product with other vector
func (v *Vector3) Dot(cur *Vector3) float64 {
	return v[0]*cur[0] +
		v[1]*cur[1] +
		v[2]*cur[2]
}

// RotateTo returns rotation matrix performing rotation (or mirroring) from unit vector to unit vector
func RotateTo(from, to *Vector3) *Matrix4 {
	to = to.Unit()
	from = from.Unit()

	if to.Copy().Sub(from).Length() < 0.001 {
		return Matrix4Identity()
	}

	c := from.Dot(to)
	v := from.Cross(to)
	h := (1 - c) / (1 - c*c)

	if math.IsInf(h, int(math.Copysign(1, h))) || math.IsNaN(h) {
		minidx := []int{0, 1, 2}

		sort.Slice(minidx, func(i, j int) bool {
			return math.Abs(from[minidx[i]]) < math.Abs(from[minidx[j]])
		})

		x := &Vector3{}

		x[minidx[0]] = 1

		u := x.Copy().Sub(from).Unit()
		v = x.Copy().Sub(to).Unit()

		ud := u.Dot(u)
		vd := v.Dot(v)
		uvd := u.Dot(v)

		ud2 := 2 / ud
		vd2 := 2 / vd
		uvd4 := 4 * uvd / (ud * vd)

		return &Matrix4{
			1 - ud2*u[0]*u[0] - vd2*v[0]*v[0] + uvd4*v[0]*u[0],
			-ud2*u[0]*u[1] - vd2*v[0]*v[1] + uvd4*v[0]*u[1],
			-ud2*u[0]*u[2] - vd2*v[0]*v[2] + uvd4*v[0]*u[2],
			0,
			-ud2*u[1]*u[0] - vd2*v[1]*v[0] + uvd4*v[1]*u[0],
			1 - ud2*u[1]*u[1] - vd2*v[1]*v[1] + uvd4*v[1]*u[1],
			-ud2*u[1]*u[2] - vd2*v[1]*v[2] + uvd4*v[1]*u[2],
			0,
			-ud2*u[2]*u[0] - vd2*v[2]*v[0] + uvd4*v[2]*u[0],
			-ud2*u[2]*u[1] - vd2*v[2]*v[1] + uvd4*v[2]*u[1],
			1 - ud2*u[2]*u[2] - vd2*v[2]*v[2] + uvd4*v[2]*u[2],
			0,
			0,
			0,
			0,
			1,
		}
	}

	return &Matrix4{
		c + h*v[0]*v[0], h*v[0]*v[1] - v[2], h*v[0]*v[2] + v[1], 0,
		h*v[0]*v[1] + v[2], c + h*v[1]*v[1], h*v[1]*v[2] - v[0], 0,
		h*v[0]*v[2] - v[1], h*v[1]*v[2] + v[0], c + h*v[2]*v[2], 0,
		0, 0, 0, 1,
	}
}
