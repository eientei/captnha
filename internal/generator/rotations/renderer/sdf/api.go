// Package sdf provides renderer.Renderer implementation using signed distance field wireframe rendering
package sdf

import (
	"image/color"
	"image/draw"
	"math"
	"runtime"
	"sync"

	"gitlab.eientei.org/eientei/captnha/internal/generator/rotations/renderer"
)

// Config parameters
type Config struct {
	Workers     int
	Origin      renderer.Vector3
	Distance    float64
	Perspective float64
	AliasStart  float64
	AliasStop   float64
	Infinity    float64
	Iterations  int
}

// DefaultConfig with default values
func DefaultConfig() *Config {
	return &Config{
		Workers:     runtime.NumCPU(),
		Origin:      renderer.Vector3{0, 0, -2},
		Distance:    18.0,
		Perspective: 4.0,
		AliasStart:  0.05,
		AliasStop:   0.005,
		Infinity:    1000,
		Iterations:  32,
	}
}

// NewRenderer returns new renderer instance and starts workers
func NewRenderer(config *Config) (renderer.Renderer, error) {
	if config == nil {
		config = DefaultConfig()
	}

	r := &rendererSDF{
		Config: *config,
		tasks:  make(chan *scanline, config.Workers),
	}

	for i := 0; i < r.Workers; i++ {
		go r.worker()
	}

	return r, nil
}

// boxframe SDF for a box frame, optimized for unit values
func boxframe(p *renderer.Vector3, s float64, e float64) (res float64) {
	p = p.Copy().Abs().Shift(-s)
	q := p.Copy().Shift(e).Abs().Shift(-e)

	if p[0] < 0 {
		p[0] = 0
	}

	if p[1] < 0 {
		p[1] = 0
	}

	if p[2] < 0 {
		p[2] = 0
	}

	if q[0] < 0 {
		q[0] = 0
	}

	if q[1] < 0 {
		q[1] = 0
	}

	if q[2] < 0 {
		q[2] = 0
	}

	a := p[0] + q[1] + q[2]
	b := q[0] + p[1] + q[2]
	c := q[0] + q[1] + p[2]

	switch {
	case a < b && a < c:
		return math.Sqrt(p[0]*p[0] + q[1]*q[1] + q[2]*q[2])
	case b < a && b < c:
		return math.Sqrt(q[0]*q[0] + p[1]*p[1] + q[2]*q[2])
	default:
		return math.Sqrt(q[0]*q[0] + q[1]*q[1] + p[2]*p[2])
	}
}

// sdf returns the step distance for given ray and topology
func sdf(p *renderer.Vector3, topology *renderer.Topology, tohit float64) (res float64) {
	p = p.Copy()

	var bs = 1.0

	var s = bs / 2

	for _, b := range topology.Boxes {
		t := boxframe(p.Copy().Add(b), s, topology.Wire)
		if t <= tohit {
			return t
		}

		if res == 0 || t < res {
			res = t
		}
	}

	return
}

// scanline a task for worker, represents single rendering row
type scanline struct {
	im       draw.Image
	mm       *renderer.Matrix4
	wg       *sync.WaitGroup
	topology *renderer.Topology
	color    func(v uint8) color.Color
	y        int
}

type rendererSDF struct {
	tasks chan *scanline
	once  sync.Once
	Config
}

// Close stops the workers
func (r *rendererSDF) Close() error {
	r.once.Do(func() {
		close(r.tasks)
	})

	return nil
}

// worker renders wired sdf topology in grayscale, with mild pseudo-antialiasing
func (r *rendererSDF) worker() {
	for task := range r.tasks {
		bounds := task.im.Bounds()
		dy := bounds.Dy()
		dx := bounds.Dx()

		yf := float64(task.y) / float64(dy)

		for x := 0; x < dx; x++ {
			xf := 1.0 - float64(x)/float64(dx)

			rd := (&renderer.Vector3{
				xf*2 - 1,
				yf*2 - 1,
				r.Perspective,
			}).Unit()

			ro := r.Origin
			ro[0] = rd[0]
			ro[1] = rd[1]
			ro[2] = rd[2] - r.Distance

			task.mm.MulVec3(rd)
			task.mm.MulVec3(&ro)

			var done bool

			var cumulative float64

			for i := 0; i < r.Iterations && !done; i++ {
				step := sdf(&ro, task.topology, r.AliasStop)

				if step < r.AliasStart {
					if step <= r.AliasStop {
						cumulative = r.AliasStart
						done = true
					} else {
						cumulative += math.Min(r.AliasStop, step)
					}
				}

				if step > r.Infinity {
					break
				}

				ro.Add(rd.Copy().Scale(step))
			}

			if cumulative > 0 {
				ox := bounds.Min.X + x
				oy := bounds.Min.Y + task.y

				d := task.im.At(ox, oy)
				s := task.color(uint8(math.Min(255.0*cumulative/r.AliasStart, 255.0)))

				task.im.Set(ox, oy, blend(d, s))
			}
		}

		task.wg.Done()
	}
}

func blend(d, s color.Color) color.Color {
	dr, dg, db, da := d.RGBA()
	sr, sg, sb, sa := s.RGBA()

	dr >>= 8
	dg >>= 8
	db >>= 8
	da >>= 8
	sr >>= 8
	sg >>= 8
	sb >>= 8
	sa >>= 8

	oa := sa + da*(255-sa)/255

	r := color.NRGBA{
		R: blendchannel(dr, sr, da, sa, oa),
		G: blendchannel(dg, sg, da, sa, oa),
		B: blendchannel(db, sb, da, sa, oa),
		A: uint8(255 - (255-da)*(255-sa)/255),
	}

	return r
}

func blendchannel(d, s, da, sa, oa uint32) uint8 {
	return uint8((s*sa + d*da*(255-sa)/255) / oa)
}

// Render submits the image rendering to workers and awaits the result
func (r *rendererSDF) Render(
	im draw.Image,
	topology *renderer.Topology,
	mm *renderer.Matrix4,
	color func(uint8) color.Color,
) {
	var wg sync.WaitGroup

	for y := 0; y < im.Bounds().Dy(); y++ {
		wg.Add(1)
		r.tasks <- &scanline{
			im:       im,
			mm:       mm,
			wg:       &wg,
			topology: topology,
			color:    color,
			y:        y,
		}
	}

	wg.Wait()
}
