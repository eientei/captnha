package renderer

import (
	"fmt"
	"math"
	"sort"
)

// VectorList list of Vector3
type VectorList []*Vector3

func (vs VectorList) String() (res string) {
	res = "renderer.VectorList{"

	for i, v := range vs {
		if i > 0 {
			res += ", "
		}

		res += v.String()
	}

	res += "}"

	return
}

// Topology represents the rendered topology of box frame coordinates, alongside with rendering parameters
type Topology struct {
	Code  string
	Boxes VectorList
	Size  float64
	Wire  float64
}

// Center updates topology boxes to become centered
func (topology *Topology) Center() {
	axis := topology.Axis()

	for i := 0; i < 3; i++ {
		shift := math.Abs(axis[i].Min-axis[i].Max) / 2
		if axis[i].Min < axis[i].Max {
			shift = -shift
		}

		if math.Abs(shift) > 0 {
			for _, b := range topology.Boxes {
				b[i] += shift
			}
		}
	}
}

// Axis of the topology
type Axis struct {
	Min       float64
	Max       float64
	Len       float64
	Weight    int
	WeightMin int
	WeightMax int
	Direction int
}

// String implementation
func (a Axis) String() string {
	return fmt.Sprintf("%d(%g:%g~%d:%d@%d)", a.Direction, a.Min, a.Max, a.WeightMin, a.WeightMax, a.Weight)
}

// Tensor returns tensor for topology vertices with preferred X/Y orientation
func (topology *Topology) Tensor() *Matrix3 {
	var res Matrix3

	axis := topology.Axis()

	idxs := []int{0, 1, 2}

	sort.Slice(idxs, func(i, j int) bool {
		return axis[idxs[i]].Len < axis[idxs[j]].Len
	})

	maxlen := max(axis[0].Len, axis[1].Len, axis[2].Len)

	bbox := []*Vector3{{}}

	bbox[0][idxs[0]] = maxlen / 2
	bbox[0][idxs[1]] = maxlen / 4

	bbox = append(bbox, topology.Boxes...)

	for _, b := range bbox {
		l := b.Length()
		if l == 0 {
			l = 1
		}

		x := b[0]
		y := b[1]
		z := b[2]

		m := l

		xs := x * x
		ys := y * y
		zs := z * z
		xy := x * y
		xz := x * z
		yz := y * z

		res[0] += m * (ys + zs)
		res[1] -= m * xy
		res[2] -= m * xz
		res[3] -= m * xy
		res[4] += m * (xs + zs)
		res[5] -= m * yz
		res[6] -= m * xz
		res[7] -= m * yz
		res[8] += m * (xs + ys)
	}

	return &res
}

// Axis determines which axis of the topology are major, minor and least important to guide the rotations
func (topology *Topology) Axis() (res []Axis) {
	var vmin, vmax Vector3

	var wmin, wmax [3]int

	for _, b := range topology.Boxes {
		for i := 0; i < 3; i++ {
			vmin[i] = math.Min(vmin[i], b[i])
			vmax[i] = math.Max(vmax[i], b[i])

			if b[i] < 0 {
				wmin[i]++
			} else if b[i] > 0 {
				wmax[i]++
			}
		}
	}

	for i := 0; i < 3; i++ {
		res = append(res, Axis{
			Min:       math.Abs(vmin[i]),
			Max:       math.Abs(vmax[i]),
			Len:       math.Abs(vmin[i]) + math.Abs(vmax[i]),
			Weight:    wmin[i] + wmax[i],
			WeightMin: wmin[i],
			WeightMax: wmax[i],
			Direction: i,
		})
	}

	return
}
