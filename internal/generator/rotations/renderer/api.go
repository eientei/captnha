// Package renderer provides topology renderer
package renderer

import (
	"image/color"
	"image/draw"
	"io"
)

// Renderer interface
type Renderer interface {
	io.Closer
	Render(im draw.Image, topology *Topology, mm *Matrix4, color func(uint8) color.Color)
}
