package renderer

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMatrix3Eigenvalues(t *testing.T) {
	m := &Matrix3{
		2, 1, 0,
		1, 3, -1,
		0, -1, 6,
	}

	vectors, values := m.Eigenspace()

	assert.EqualValues(t, &Vector3{6.322802452973998, 3.3585280981543573, 1.3186694488716209}, values)

	assert.EqualValues(
		t,
		&Matrix3{
			-0.07906883710997453, -0.3178766306313138, 0.944829384966703,
			0.566325101493612, 0.7656723077537302, 0.3049947484094185,
			0.8203803986056255, -0.5591961774167978, -0.1194806961233295,
		},
		vectors,
	)
}

func TestMatrix3QR(t *testing.T) {
	m := &Matrix3{
		12, -51, 4,
		6, 167, -68,
		-4, 24, -41,
	}

	Q, R := m.QR()

	assert.EqualValues(
		t,
		&Matrix3{
			0.8571428571428571, -0.3942857142857144, 0.3314285714285714,
			0.4285714285714286, 0.9028571428571429, -0.03428571428571431,
			-0.28571428571428575, 0.17142857142857107, 0.9428571428571428,
		},
		Q,
	)

	assert.EqualValues(
		t,
		&Matrix3{
			13.999999999999998, 21.00000000000001, -14.000000000000004,
			-1.1102230246251565e-16, 175, -69.99999999999999,
			-4.440892098500626e-16, -3.552713678800501e-15, -35,
		},
		R,
	)
}

func TestRotateTo(t *testing.T) {
	from := &Vector3{0, 1, 0}
	to := &Vector3{0, 0, 1}

	assert.EqualValues(t, to, RotateTo(from, to).MulVec3(from.Copy()))

	from = &Vector3{0, 1, 0}
	to = (&Vector3{0, 0, 0.9}).Unit()

	assert.EqualValues(t, to, RotateTo(from, to).MulVec3(from.Copy()))

	from = &Vector3{0, 1, 0}
	to = (&Vector3{0, -1, 0}).Unit()

	assert.EqualValues(t, to, RotateTo(from, to).MulVec3(from.Copy()))
}
