package renderer

import (
	"math"
)

// Matrix3 3x3 matrix type
type Matrix3 [9]float64

// Matrix3Identity returns identity matrix
func Matrix3Identity() *Matrix3 {
	return &Matrix3{
		1, 0, 0,
		0, 1, 0,
		0, 0, 1,
	}
}

// Scale matrix scale
func (m *Matrix3) Scale(v float64) *Matrix3 {
	return &Matrix3{
		m[0] * v, m[1] * v, m[2] * v,
		m[3] * v, m[4] * v, m[5] * v,
		m[6] * v, m[7] * v, m[8] * v,
	}
}

// Add matrix sum
func (m *Matrix3) Add(o *Matrix3) *Matrix3 {
	return &Matrix3{
		m[0] + o[0], m[1] + o[1], m[2] + o[2],
		m[3] + o[3], m[4] + o[4], m[5] + o[5],
		m[6] + o[6], m[7] + o[7], m[8] + o[8],
	}
}

// Sub matrix difference
func (m *Matrix3) Sub(o *Matrix3) *Matrix3 {
	return &Matrix3{
		m[0] - o[0], m[1] - o[1], m[2] - o[2],
		m[3] - o[3], m[4] - o[4], m[5] - o[5],
		m[6] - o[6], m[7] - o[7], m[8] - o[8],
	}
}

// MulVec returns new multiplication matrix with the result of multiplication
func (m *Matrix3) MulVec(o *Vector3) *Vector3 {
	return &Vector3{
		m[0]*o[0] + m[1]*o[1] + m[2],
		m[3]*o[0] + m[4]*o[1] + m[5],
		m[6]*o[0] + m[7]*o[1] + m[8],
	}
}

// MulMat3 performs in-place matrix multiplication
func (m *Matrix3) MulMat3(o *Matrix3) *Matrix3 {
	m0 := m[0]*o[0] + m[1]*o[3] + m[2]*o[6]
	m1 := m[0]*o[1] + m[1]*o[4] + m[2]*o[7]
	m2 := m[0]*o[2] + m[1]*o[5] + m[2]*o[8]
	m3 := m[3]*o[0] + m[4]*o[3] + m[5]*o[6]
	m4 := m[3]*o[1] + m[4]*o[4] + m[5]*o[7]
	m5 := m[3]*o[2] + m[4]*o[5] + m[5]*o[8]
	m6 := m[6]*o[0] + m[7]*o[3] + m[8]*o[6]
	m7 := m[6]*o[1] + m[7]*o[4] + m[8]*o[7]
	m8 := m[6]*o[2] + m[7]*o[5] + m[8]*o[8]

	m[0] = m0
	m[1] = m1
	m[2] = m2
	m[3] = m3
	m[4] = m4
	m[5] = m5
	m[6] = m6
	m[7] = m7
	m[8] = m8

	return m
}

// Transpose performs in-place transposition
func (m *Matrix3) Transpose() *Matrix3 {
	m[1], m[3] = m[3], m[1]
	m[2], m[6] = m[6], m[2]
	m[5], m[7] = m[7], m[5]

	return m
}

// QR returns QR decomposition
func (m *Matrix3) QR() (matq, matr *Matrix3) {
	var qs []*Matrix3

	z := m

	for k := 0; k < 2; k++ {
		for i := 0; i < k; i++ {
			z[i*3+i] = 1

			for d := i + 1; d < 3; d++ {
				z[i*3+d] = 0
				z[d*3+i] = 0
			}
		}

		mcol := Vector3{z[0+k], z[3+k], z[6+k]}

		a := mcol.Length()
		if m[k*3+k] > 0 {
			a = -a
		}

		e := mcol

		e[k] += a

		e.Unit()

		q := &Matrix3{
			1 + -2*e[0]*e[0], -2 * e[0] * e[1], -2 * e[0] * e[2],
			-2 * e[1] * e[0], 1 + -2*e[1]*e[1], -2 * e[1] * e[2],
			-2 * e[2] * e[0], -2 * e[2] * e[1], 1 + -2*e[2]*e[2],
		}

		qs = append(qs, q)

		z = q.Copy().MulMat3(z)
	}

	matq = qs[1].MulMat3(qs[0])

	matr = matq.Copy().MulMat3(m)

	matq = matq.Transpose()

	return
}

// Eigenspace returns eigenvectors and eigenvalues for matrix using QR decomposition
func (m *Matrix3) Eigenspace() (eivec *Matrix3, values *Vector3) {
	A := m

	eivec = Matrix3Identity()
	values = &Vector3{}

	for i := 0; i < 20; i++ {
		Q, R := A.QR()
		A = R.MulMat3(Q)

		eivec = eivec.MulMat3(Q)

		if A.Diagonal() {
			break
		}
	}

	for i := 0; i < 3; i++ {
		values[i] = A[i*3+i]
	}

	eivec = eivec.Transpose()

	for i := range eivec {
		eivec[i] = -eivec[i]
	}

	return
}

// Diagonal returns true if matrix doesn't have off-diagonal nonzero values
func (m *Matrix3) Diagonal() bool {
	var found bool

	for i := 0; i < 3 && !found; i++ {
		for d := i + 1; d < 3; d++ {
			if m[i*3+d] != 0 || m[d*3+i] != 0 {
				found = true

				break
			}
		}
	}

	return !found
}

// Copy returns independent instance of the same matrix
func (m *Matrix3) Copy() *Matrix3 {
	return &Matrix3{
		m[0], m[1], m[2],
		m[3], m[4], m[5],
		m[6], m[7], m[8],
	}
}

// Matrix4 4x4 matrix type
type Matrix4 [16]float64

// Matrix4Identity returns identity matrix
func Matrix4Identity() *Matrix4 {
	return &Matrix4{
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1,
	}
}

// MulVec3 performs in-place multiplication of 3-vector with other 3-vector
func (m *Matrix4) MulVec3(o *Vector3) *Vector3 {
	a := o[0]*m[0] + o[1]*m[1] + o[2]*m[2] + m[3]
	b := o[0]*m[4] + o[1]*m[5] + o[2]*m[6] + m[7]
	c := o[0]*m[8] + o[1]*m[9] + o[2]*m[10] + m[11]

	o[0] = a
	o[1] = b
	o[2] = c

	return o
}

// MulMat4 returns new multiplication matrix with the result of multiplication
func (m *Matrix4) MulMat4(o *Matrix4) *Matrix4 {
	return &Matrix4{
		m[0]*o[0] + m[1]*o[4] + m[2]*o[8] + m[3]*o[12],
		m[0]*o[1] + m[1]*o[5] + m[2]*o[9] + m[3]*o[13],
		m[0]*o[2] + m[1]*o[6] + m[2]*o[10] + m[3]*o[14],
		m[0]*o[3] + m[1]*o[7] + m[2]*o[11] + m[3]*o[15],
		m[4]*o[0] + m[5]*o[4] + m[6]*o[8] + m[7]*o[12],
		m[4]*o[1] + m[5]*o[5] + m[6]*o[9] + m[7]*o[13],
		m[4]*o[2] + m[5]*o[6] + m[6]*o[10] + m[7]*o[14],
		m[4]*o[3] + m[5]*o[7] + m[6]*o[11] + m[7]*o[15],
		m[8]*o[0] + m[9]*o[4] + m[10]*o[8] + m[11]*o[12],
		m[8]*o[1] + m[9]*o[5] + m[10]*o[9] + m[11]*o[13],
		m[8]*o[2] + m[9]*o[6] + m[10]*o[10] + m[11]*o[14],
		m[8]*o[3] + m[9]*o[7] + m[10]*o[11] + m[11]*o[15],
		m[12]*o[0] + m[13]*o[4] + m[14]*o[8] + m[15]*o[12],
		m[12]*o[1] + m[13]*o[5] + m[14]*o[9] + m[15]*o[13],
		m[12]*o[2] + m[13]*o[6] + m[14]*o[10] + m[15]*o[14],
		m[12]*o[3] + m[13]*o[7] + m[14]*o[11] + m[15]*o[15],
	}
}

// EulerAngles returns euler angle sets leading to current rotation
func (m *Matrix4) EulerAngles() []*Vector3 {
	if math.Abs(m[8]) != 1 {
		y1 := -math.Asin(m[8])
		y2 := math.Pi - y1
		cy1 := math.Cos(y1)
		cy2 := math.Cos(y2)
		x1 := math.Atan2(m[9]/cy1, m[10]/cy1)
		x2 := math.Atan2(m[9]/cy2, m[10]/cy2)
		z1 := math.Atan2(m[4]/cy1, m[0]/cy1)
		z2 := math.Atan2(m[4]/cy2, m[0]/cy2)

		return []*Vector3{
			{x1, y1, z1},
			{x2, y2, z2},
		}
	}

	z := 0.0

	var x, y float64

	if m[8] == -1 {
		y = math.Pi / 2
		x = z + math.Atan2(m[1], m[2])
	} else {
		y = -math.Pi / 2
		x = -z + math.Atan2(-m[1], -m[2])
	}

	return []*Vector3{
		{x, y, z},
	}
}

// RotateX returns 4x4 matrix with euler rotation around X axis
func RotateX(a float64) *Matrix4 {
	c := math.Cos(a)
	s := math.Sin(a)

	return &Matrix4{
		1, 0, 0, 0,
		0, c, -s, 0,
		0, s, c, 0,
		0, 0, 0, 1,
	}
}

// RotateY returns 4x4 matrix with euler rotation around Y axis
func RotateY(a float64) *Matrix4 {
	c := math.Cos(a)
	s := math.Sin(a)

	return &Matrix4{
		c, 0, s, 0,
		0, 1, 0, 0,
		-s, 0, c, 0,
		0, 0, 0, 1,
	}
}

// RotateZ returns 4x4 matrix with euler rotation around Z axis
func RotateZ(a float64) *Matrix4 {
	c := math.Cos(a)
	s := math.Sin(a)

	return &Matrix4{
		c, -s, 0, 0,
		s, c, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1,
	}
}

// Rotate dispatches the rotation using index of [X,Y,Z] axis
func Rotate(axis int, rads float64) *Matrix4 {
	if axis < 0 {
		axis = -axis
		rads = -rads
	}

	switch axis {
	case 0:
		return RotateX(rads)
	case 1:
		return RotateY(rads)
	default:
		return RotateZ(rads)
	}
}
