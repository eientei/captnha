// Package captnha provides CAPTNHA implementation - Completely Automated Public Test to tell Niggers and Humans Apart
package captnha

import (
	"crypto/md5"
	"crypto/rand"
	"encoding/hex"
	"encoding/json"
	"io"
	"io/fs"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"time"

	"gitlab.eientei.org/eientei/captnha/internal/generator"
)

// Config for CAPTNHA
type Config struct {
	Generator       generator.Generator
	Directory       string
	Cleanup         time.Duration
	BandwidthPeriod time.Duration
	BandwidthBytes  int
}

// NewCAPTNHA returns new CAPTNHA instance
func NewCAPTNHA(config *Config) (*CAPTNHA, error) {
	c := &CAPTNHA{
		Config:          *config,
		once:            sync.Once{},
		done:            make(chan struct{}),
		bandwidthFactor: 0,
	}

	if config.BandwidthBytes > 0 {
		c.bandwidthFactor = config.BandwidthPeriod / time.Duration(config.BandwidthBytes)
	}

	err := filepath.WalkDir(config.Directory, c.walker)
	if err != nil {
		return nil, err
	}

	go c.cleanup()

	return c, nil
}

// CAPTNHA HTTP handler
type CAPTNHA struct {
	done chan struct{}
	Config
	bandwidthFactor time.Duration
	once            sync.Once
}

type response struct {
	MD5   string `json:"md5"`
	Token string `json:"token"`
	URL   string `json:"url"`
}

func (impl *CAPTNHA) cleanup() {
	ticker := time.NewTicker(impl.Config.Cleanup)

	defer ticker.Stop()

	for {
		select {
		case <-impl.done:
			return
		case <-ticker.C:
			_ = filepath.WalkDir(impl.Directory, impl.walker)
		}
	}
}

func (impl *CAPTNHA) walker(path string, d fs.DirEntry, err error) error {
	if err != nil {
		return err
	}

	if d.IsDir() && path != impl.Directory {
		return filepath.SkipDir
	}

	if !strings.HasSuffix(path, ".png") {
		return nil
	}

	info, err := d.Info()
	if err != nil {
		return err
	}

	if time.Since(info.ModTime()) < impl.Config.Cleanup {
		return nil
	}

	return os.Remove(path)
}

// Close implementation, stops cleanup
func (impl *CAPTNHA) Close() error {
	impl.once.Do(func() {
		close(impl.done)
	})

	return nil
}

func (impl *CAPTNHA) serveFile(
	w http.ResponseWriter,
	r *http.Request,
	name string,
	modtime time.Time,
	f io.ReadSeeker,
) {
	if impl.bandwidthFactor != 0 {
		f = &bandwidthLimitedReadSeeker{
			ReadSeeker: f,
			bytes:      impl.Config.BandwidthBytes,
			factor:     impl.bandwidthFactor,
		}
	}

	http.ServeContent(w, r, name, modtime, f)
}

// ServeHTTP HTTP handler implementation
func (impl *CAPTNHA) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)

		return
	}

	if strings.HasPrefix(r.URL.Path, "/captnhas/") {
		name := filepath.Base(strings.TrimPrefix(r.URL.Path, "/captnhas/"))
		path := filepath.Join(impl.Directory, name)

		finfo, err := os.Stat(path)
		if os.IsNotExist(err) {
			http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)

			return
		}

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)

			return
		}

		if finfo.IsDir() {
			http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)

			return
		}

		f, err := os.Open(path)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)

			return
		}

		defer func() {
			_ = f.Close()
		}()

		impl.serveFile(w, r, name, finfo.ModTime(), f)

		return
	}

	if r.URL.Path != "/new" {
		http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)

		return
	}

	idbs := make([]byte, 16)

	_, err := rand.Read(idbs)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	id := hex.EncodeToString(idbs)

	name := id + ".png"

	path := filepath.Join(impl.Directory, name)

	f, err := os.OpenFile(path, os.O_CREATE|os.O_TRUNC|os.O_RDWR, 0666)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	defer func() {
		_ = f.Close()
	}()

	code, err := impl.Generator.Generate(f)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	if r.Header.Get("accept") == "image/png" {
		impl.serveFile(w, r, name, time.Now(), f)

		return
	}

	md5sum := md5.Sum(([]byte)(code))

	bs, err := json.Marshal(
		response{
			MD5:   hex.EncodeToString(md5sum[:]),
			Token: id,
			URL:   "/captnhas/" + name,
		},
	)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	w.Header().Set("content-type", "application/json")
	w.Header().Set("content-length", strconv.Itoa(len(bs)))

	w.WriteHeader(http.StatusOK)

	_, _ = w.Write(bs)
}

type bandwidthLimitedReadSeeker struct {
	io.ReadSeeker
	bytes  int
	factor time.Duration
}

func (b *bandwidthLimitedReadSeeker) Read(p []byte) (n int, err error) {
	if len(p) > b.bytes {
		p = p[:b.bytes]
	}

	start := time.Now()

	n, err = b.ReadSeeker.Read(p)

	time.Sleep(time.Duration(n)*b.factor - time.Since(start))

	return
}
