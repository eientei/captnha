FROM scratch
ARG BIN
COPY ./$BIN /captnha
VOLUME /captnhas
ENTRYPOINT ["/captnha"]
