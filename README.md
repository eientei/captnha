# CAPTNHA

[Binary builds](https://gitlab.eientei.org/eientei/captnha/-/releases)
[Docker builds](https://gitlab.eientei.org/eientei/captnha/container_registry/1)

## Completely Automated Public Test to tell Niggers and Humans Apart

Provides a simple test that requires cognitive capabilities at a median or above level to solve.

Can serve as a CAPTCHA as well.

API is compatible with [kocaptcha](https://github.com/koto-bank/kocaptcha)

See [doc](./doc) for sample configuration files. 

### Rotations
A spatial reasoning quiz featuring wireframe box figures at various configurations and rotations, 
requiring user to find two identical figures.

Example:

![rotations sample](./examples/rotations.png)
